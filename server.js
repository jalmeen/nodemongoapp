var express = require('express');
var mongodb = require("mongodb");
var bodyParser= require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

app.set('port', (process.env.PORT || 5000));

var uristring = process.env.MONGODB_URI || 'mongodb://localhost:27017';

app.set('views', __dirname + '/views');

app.get('/', function(request, response) {
  //response.render('/index');
  response.sendFile(__dirname + '/views/index.html');
});

app.get('/success', function(request, response) {
  //response.render('/index');
  response.sendFile(__dirname + '/views/success.html');
});

app.post('/form', (req, res) => {
  db.collection('sample').save(req.body, (err, result) => {
    if (err){
	console.log(req.body)
    console.log('unable to save to db')
    res.send("failed saving on server");
    }else{
	console.log(req.body)
    console.log('saved to database')
    res.redirect("/success");
    }
  })
})

app.get('/all', (req, res) => {
  db.collection('sample').find().toArray(function(err, results) {
  console.log(results)
  // send HTML file populated with quotes here
  res.json(results);

})

})// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
var db;

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(uristring, function (err, database) {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  // Save database object from the callback for reuse.
  db = database;
  console.log("Database connection ready");

  // Initialize the app.
  var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
  });
});
