function submitData(){

	//validate fields
	if(validateFields()){

	postData();
	}else{
   //alert("validation failed");
	}
}
function validateFields() {

     var teamname = document.getElementById("teamname").value;
     var projectname = document.getElementById("projectname").value;
     if( teamname ==='' || projectname === ''){
       alert("Please fill all fields.");
     return false;
	 }
		 else{
     return true;
 }
}
function postData() {

  var teamname = document.getElementById("teamname").value;
  var projectname = document.getElementById("projectname").value;
  // collect the form data while iterating over the inputs
  var data = {"teamname":teamname,"projectname":projectname};
  // construct an HTTP request
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/welcome", true);
  xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

  // send the collected data as JSON
  xhr.send(JSON.stringify(data));
  console.log(JSON.stringify(data));
  xhr.onloadend = function (e) {
    // done
        window.location="/success";
  };
}
